; Programming Assignment 3
; author: Brannon Brakefield
; date: 11/2016
.586
.MODEL FLAT
.STACK 4096
.DATA
stringIn    BYTE    80 DUP(?)
stringOut   BYTE    80 DUP (?)
limit       DWORD   80 DUP (?)
sum         DWORD    0
hexMap      BYTE    "0123456789ABCDEF",0



.CODE

_strcopyx     PROC    NEAR32
 ; This procedure will copy a string until the null byte in the source is copied.
 ; The destination location is assumed to be long enough to hold the copy.
 ; Params:
 ; (1)  address of destination
 ; (2)  address of source

                    push    ebp
                    mov     ebp, esp
                    mov     eax, [ebp + 12]         ; load source string into eax
                    lea     esi, [eax]              ; source
                    mov     eax, [ebp + 8]          ; load destination into eax
                    lea     edi, [eax]              ; destinaton address
                    ;mov    eax, [ebp + 16]         ; length

                    mov eax, 0

            loop_len:
                    mov edx, [edi]                  ; move the value stored at edi's address
                    cmp edx, 0                      ; compare to zero
                    jz done                         ; if zero jump to done
                
                    inc edi                         ; increment edi
                    inc eax                         ; increment length
                    jmp loop_len                    ; loop back
            done:
                    mov eax, [ebp + 16]             ; length of destination string
                    mov ebx, eax                    ; move length into ebx          
                    mov ecx, 0
                    mov eax, [ebp + 8]
                    lea edi, [eax]                     ; reload the destination string into edi

        whileNoNull:
                    cmp     ebx, ecx                ; compare length to counter
                    je      endWhileNoNull          ; jump to end if equal
                    cmp     BYTE PTR [esi],0        ; null source byte
                    je      endWhileNoNull          ; stop copying if null
                    movsb                           ; copy one byte
                    inc     ecx                     ; increment length coutner
                    jmp     whileNoNull             ; check the next byte

    endWhileNoNull:
                    mov     BYTE PTR [edi],0        ; terminate destination string
              
                    mov     esp, ebp
                    pop     ebp
                    mov eax,0   
                    ret                             ; return
_strcopyx           ENDP

_strncopyx          PROC    NEAR32
 ; This procedure will copy a string until the null byte in the source is copied
 ; or until the specified number of bytes to copy has been copied.
           
                    push    ebp
                    mov     ebp, esp
                    mov     eax, [ebp + 8]          ; load destination string             
                    lea     edi, [eax]              ; destination address
                    mov     eax, [ebp + 12]         ; load source string
                    lea     esi, [eax]              ; source address
                    mov     eax, [ebp + 16]
                    mov     limit, eax

                    xor eax, eax

            loop_len:
                    mov edx, [edi]
                    cmp edx, 0
                    jz done

                    inc edi
                    inc eax
                    jmp loop_len
              done:
                    mov ebx, eax
                    mov ecx, 0
                    mov     eax, [ebp + 8]          ; reload destination string             
                    lea     edi, [eax]
                    ;atod   limit
                    mov eax, limit

        whileNotLimit:
                    cmp     eax, 0
                    je      endWhileNoNull    
                    cmp     ebx, ecx
                    je      endWhileNoNull
            
        whileNoNull:
                    cmp     BYTE PTR [esi],0
                    je      endWhileNoNull
                    movsb
                    inc     ecx
                    dec     eax                     ; increment towards limit
                    jmp     whileNotLimit

     endWhileNoNull:
                    mov BYTE PTR [edi],0
                    mov     esp, ebp
                    pop     ebp
                    mov eax,0  
                    ret                             ; return
_strncopyx          ENDP


_strcatx            PROC

                    push    ebp
                    mov     ebp, esp
                    mov     eax, [ebp + 8]          ; load destination string             
                    lea     edi, [eax]              ; destination address
                    mov     eax, [ebp + 12]         ; load source string
                    lea     esi, [eax]              ; source address
                    xor eax, eax

        loop_len:
                    cmp BYTE PTR [edi], 0
                    jz done
    
                    inc edi
                    inc eax
                    jmp loop_len
        done:
                    mov ebx, eax 
            
        whileNoNull:
                    cmp     BYTE PTR [esi],0
                    je      endWhileNoNull
                    movsb
                    jmp     whileNoNull

        endWhileNoNull:
                    mov BYTE PTR [edi],0
                    mov     esp, ebp
                    pop     ebp
                    mov eax,0  
                    ret                             ; return
_strcatx            ENDP






_binToHexStr        PROC

                    push    ebp
                    mov     ebp, esp
                    mov     eax, [ebp + 8]          ; load binary input string
                    lea     esi, [eax]
                    xor eax, eax                             ; eax = 0

        loop_len:
                    cmp BYTE PTR [esi], 0           ; compare value in esi index to 0
                    jz len_done                     ; if zero jump to len_done
    
                    inc esi                         ; increment through string
                    inc eax                         ; increase length by one
                    jmp loop_len                    ; continue calculating length
        len_done:
                    mov ebx, eax                    ; ebx = length
                    dec esi                         ; decrement esi to exclude null terminator
                                                    
                    mov ecx, ebx                    ; preserve length
                    mov edx, 0                      ; move zero in edx to prepare as counter
                    mov eax, 0

        checkIfOne:
                    cmp  BYTE PTR [esi], '0'
                    je notEqualOne
                    ;atod BYTE PTR [esi]          ; convert ascii to number value
                    mov eax, 1
        exLoop:
                    cmp ebx, 0
                    je done2
                    cmp edx, 0
                    je expZero

                    mov ecx, edx
                    mulByTwo:
                    cmp ecx, 0
                    je exDone
                    imul eax, 2
                    dec ecx
                    jmp mulByTwo
                       
        exDone:
                    add sum, eax
                    jmp notEqualOne
                expZero:
                    mov eax, 1
                    add sum, eax
                    mov sum, eax
        notEqualOne:
                    dec esi
                    dec ebx
                    inc edx
                    cmp ebx, 0
                    jne checkIfOne
        done2:
                    mov eax, sum
                    mov ecx, 16

                    lea edi, stringOut
                    ;mov edx, 0
                    
        divLoop:
                    mov edx, 0
                    cmp eax, 0
                    jz  divDone
                    div ecx
                    mov dl, [hexMap + edx]
                    mov BYTE PTR [edi], dl
                    inc edi
                    jmp divLoop
        divDone:

                    mov BYTE PTR [edi], 0
                    lea esi, stringOut
                    mov eax, [ebp + 16]
                    lea edi, [eax]
                    mov eax, 0
                    ;lea edi, limit

        loop_len2:
                    cmp BYTE PTR [esi], 0           ; compare value in esi index to 0
                    jz len_done2                     ; if zero jump to len_done
    
                    inc esi                         ; increment through string
                    inc eax                         ; increase length by one
                    jmp loop_len2                    ; continue calculating length
        len_done2:
                    mov ebx, eax                    ; ebx = length
                    dec esi                         ; decrement esi to exclude null terminator
                                                        ; decrement ebx to exclude null terminator in length value
                    mov ecx, ebx                     ; move zero into ecx to prepare as counter
                
        copyLoop:
                    cmp ecx, 0
                    je  copyLoopDone
                    movsb
                    sub esi, 2
                    dec ecx
                    jmp copyLoop
                        
        copyLoopDone:
                    mov BYTE PTR [edi], 0
                    mov     esp, ebp
                    pop     ebp
                    mov eax, 0
                    ret

_binToHexStr            ENDP

_hexStrToBin        PROC

                    push    ebp
                    mov     ebp, esp
                    mov eax, [ebp + 8] ; hex string
                    lea esi, [eax]
                    mov eax, 0
                    mov sum, eax
                    xor eax, eax                             ; eax = 0

            loop_len:
                    cmp BYTE PTR [esi], 0           ; compare value in esi index to 0
                    jz len_done                     ; if zero jump to len_done
    
                    inc esi                         ; increment through string
                    inc eax                         ; increase length by one
                    jmp loop_len                    ; continue calculating length
            len_done:
                    mov ebx, eax                    ; ebx = length
                    dec esi                         ; decrement esi to exclude null terminator

                    mov ecx, ebx                    ; mov length into ecx                    
                    mov edx, 0
                    mov eax, 0
                    

            loopForLength:
                    mov edx, 0                      ; hex map counter
                    cmp ecx, 0
                    
                    je loopForLengthDone

                hexLoop:
                    mov al, [hexMap + edx]
                    cmp BYTE PTR [esi], al
                    jne notEqual
      
                    mov eax, edx
                    mov edx, ebx
                    sub edx, ecx
                    

                    cmp edx, 0
                    jnz notZero
                    jmp addSum

                notZero:
                    cmp edx, 0
                    je addSum
                    sal eax, 4
                   
                    dec edx
                    jmp notZero

                addSum:
                    add sum, eax
                    dec esi
                    dec ecx
                    jmp loopForLength

                notEqual:
                    inc edx
                    jmp hexLoop

            loopForLengthDone:
                  
                    mov eax, sum
                    mov ebx, 2
                    lea esi, stringIn
                div2:
                    mov edx, 0
                    cmp eax, 0
                    je div2Done
                    div ebx
                    mov dl, [hexMap + edx]
                    mov BYTE PTR [esi], dl
                    inc esi
                    jmp div2

                div2Done:

                    mov BYTE PTR [esi], 0
                        
                    lea esi, stringIn

                    mov eax, [ebp + 12]
                    lea edi, [eax]
                    mov eax, 0
                    ;lea edi, limit

        loop_len2:
                    cmp BYTE PTR [esi], 0           ; compare value in esi index to 0
                    jz len_done2                     ; if zero jump to len_done
    
                    inc esi                         ; increment through string
                    inc eax                         ; increase length by one
                    jmp loop_len2                    ; continue calculating length
        len_done2:
                    mov ebx, eax                    ; ebx = length
                    dec esi                         ; decrement esi to exclude null terminator
                                                        ; decrement ebx to exclude null terminator in length value
                    mov ecx, ebx                     ; move zero into ecx to prepare as counter
                
        copyLoop:
                    cmp ecx, 0
                    je  copyLoopDone
                    movsb
                    sub esi, 2
                    dec ecx
                    jmp copyLoop
                        
        copyLoopDone:
                    mov BYTE PTR [edi], 0
                    mov     esp, ebp
                    pop     ebp
                    mov eax, 0
                    ret
_hexStrToBin            ENDP   
END




