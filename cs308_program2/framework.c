#include <windows.h>
#include <stdio.h>
#include "resource.h" 

void strcopyx (char* a, const char* b, int a_len);
void strncopyx(char* a, const char* b, int b_len, int a_len);
void strcatx(char* a, const char* b, int a_len);
void binToHexStr(unsigned char* binbuf,
                 int binbuf_len,
                 char* strbuf,
                 int strbuf_len);

void hexStrToBin(char* hexStr,
                unsigned char* binbuf,
                int binbuf_len);

static char buf[255];
static char inputLabel[255];

#pragma warning(disable : 4996)
// disables warning for strcpy use

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
    char dest_string[12] = "test";
    const char source_string[] = "foobar";
    unsigned char b_input[] = {0x31,0x31,0x31,0x30,0x30,0x31,0x00};
    char hexString[] = "10BA";
    unsigned char binbuf[80];
    char hexbuf[80];

	AllocConsole();
	freopen("CONIN$" , "rb", stdin);
	freopen("CONOUT$" , "wb", stdout);
   
    puts("Testing strcpyx...");
    puts("Destination String:");
    puts(dest_string);
    puts("Source String:");
    puts(source_string);
	strcopyx(dest_string, source_string, strlen(dest_string));
    puts("Destination String after processing:");
    puts(dest_string);
    puts("\n");
    strcpy(dest_string, "test");
    
    puts("Testing strncpyx...");
    puts("Destination String:");
    puts(dest_string);
    puts("Source String:");
    puts(source_string);
    puts("Copying 3 characters...");
    strncopyx(dest_string, source_string, 3, strlen(dest_string));
    puts("Destination String after processing:");
    puts(dest_string);
    puts("\n");
    strcpy(dest_string, "test");

    puts("Testing strcatx...");
    puts("Destination String:");
    puts(dest_string);
    puts("Source String:");
    puts(source_string);
    strcatx(dest_string, source_string,strlen(dest_string));
    puts("Destination String after processing:");
    puts(dest_string);
    strcpy(dest_string, "test");
    puts("\n");
    
    puts("Testing binToHexString...");
    puts("Binary String:");
    puts(b_input);
    binToHexStr(b_input, 6,hexbuf, 80);
    puts("Resulting Hex String:");
    puts(hexbuf);
    puts("\n");

    puts("Testing hexStringToBinary...");
    puts("Hex String:");
    puts(hexString);
    hexStrToBin(hexString, binbuf,80);
    puts("Resulting Binary String:");
    puts(binbuf);
    system("pause");
}


